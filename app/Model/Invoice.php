<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('invoice_client_type', 
                                'account_type',
                                'account_id', 
                                'first_name',
                                'last_name', 
                                'company',
                                'email', 
                                'phone',
                                'cell_phone', 
                                'address',
                                'address_2', 
                                'city',
                                'state', 
                                'postal_code',
                                'country'
                            );
}
