<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_type' => 'required',
            'account_id'   => 'required',
            'first_name'   => 'required|alpha',
            'last_name'    => 'required|alpha',
            'company'      => 'required',
            'email'        => 'required|email',
            'phone'        => 'required',
            'cell_phone'   => 'required',
            'address'      => 'required',
            'city'         => 'required|alpha',
            'postal_code'  => 'required',
            'state'        => 'required|alpha',
            'country'      => 'required|alpha'
        ];
    }
}
