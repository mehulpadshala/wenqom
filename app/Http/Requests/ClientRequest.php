<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status'       => 'required',
            'account_type' => 'required',
            'first_name'   => 'required',
            'last_name'    => 'required',
            'company'      => 'required',
            'email'        => 'required|email',
            'phone'        => 'required',
            'cell_phone'   => 'required',
            'address'      => 'required',
            'address_2'    => 'required',
            'city'         => 'required',
            'postal_code'  => 'required',
            'state'        => 'required|email',
            'country'      => 'required',
            'password'     => 'required',
            'billing_first_name' => 'required',
            'billing_last_name'  => 'required',
            'billing_company'    => 'required',
            'billing_email'      => 'required|email',
            'billing_phone'      => 'required',
            'billing_cell_phone' => 'required',
            'billing_address'    => 'required',
            'billing_address_2'  => 'required',
            'billing_city'       => 'required',
            'billing_postal_code'=> 'required|email',
            'billing_phone'      => 'required',
            'billing_state'      => 'required',
            'billing_country'    => 'required',
        ];
    }
}
