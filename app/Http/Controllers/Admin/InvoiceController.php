<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Invoice;
use App\Http\Requests\InvoiceRequest;
use Session;

class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::all();
        
        //load the invoice listing view
        return view('admin.invoice.index')->with('invoices', $invoices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.invoice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvoiceRequest $request)
    {
        $invoice = new Invoice();
        
        $invoice->account_type = $request->account_type;
        $invoice->account_id   = $request->account_id;
        $invoice->first_name   = $request->first_name;
        $invoice->last_name    = $request->last_name;
        $invoice->company      = $request->company;
        $invoice->email        = $request->email;
        $invoice->phone        = $request->phone;
        $invoice->cell_phone   = $request->cell_phone;
        $invoice->address      = $request->address;
        $invoice->address_2    = $request->address_2;
        $invoice->city         = $request->city;
        $invoice->postal_code  = $request->postal_code;
        $invoice->state        = $request->state;
        $invoice->country      = $request->country;

        $isAddedInvoice = $invoice->save();
        //pred($isAddedInvoice);

        if($isAddedInvoice) {
            // redirect
            Session::flash('success', 'Successfully invoice saved !');
            return redirect('admin/invoice');
        } else {
            Session::flash('error', 'Sorry, invoice not saved !');
            return redirect('admin/invoice/create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
