<?php

function pre() {
    
    $args = func_get_args();
    if($args) {
        foreach ($args as $key => $arg) {
            echo "<pre>";
            print_r($arg);
            echo "</pre>";
        }
    }
}

function pred() {
    
     call_user_func_array('pre', func_get_args());
     die();
}

function ver() {
    
    $args = func_get_args();
    if($args) {
        foreach ($args as $key => $arg) {
            echo "<pre>";
            var_dump($arg);
            echo "</pre>";
        }
    }
}

function verd() {
    
     call_user_func_array('ver', func_get_args());
     die();
}

/**
 * Return full images url.
 * 
 * @param string $imgName
 * @return string
 */
function imgUrl($imgName)
{
    return asset('assets/images/'. $imgName);
}