@if(Auth::check())
    <div class="page-footer">
        <div class="copyright"><span class="text-15px">2016 © 
        	<a href="javascript:void(0);" target="_blank">
        		Webqom Technologies Sdn Bhd.
        	</a> Any queries, please contact 
        	<a href='mailto:support@webqom.com'>Webqom Support</a>.</span>
            <div class="pull-right"><img src="{{ imgUrl('logo_webqom.png')}}" alt="Webqom Technologies"></div>
        </div>
    </div>
@endif

{!! Html::script('assets/js/jquery-1.9.1.js') !!}
{!! Html::script('assets/js/jquery-migrate-1.2.1.min.js') !!}
{!! Html::script('assets/js/jquery-ui.js') !!}
{!! Html::script('assets/vendors/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('assets/vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js') !!}
{!! Html::script('assets/js/html5shiv.js') !!}
{!! Html::script('assets/js/respond.min.js') !!}
{!! Html::script('assets/vendors/jquery-cookie/jquery.cookie.js') !!}

@yield('script')