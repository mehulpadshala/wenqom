<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ imgUrl('icons/favicon.ico') }}" rel="shortcut icon">
    <title> @yield('title')</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="{{ imgUrl('icons/favicon.ico') }}" rel="shortcut icon">
    
    <!--Loading bootstrap css-->
    <link type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,700,400italic,700italic' rel='stylesheet' type='text/css'>
    
    {!! Html::style('assets/vendors/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css') !!}
    {!! Html::style('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') !!}
    {!! Html::style('assets/vendors/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') !!}
    {!! Html::style('assets/vendors/animate.css/animate.css') !!}
    {!! Html::style('assets/vendors/jquery-pace/pace.css') !!}
    {!! Html::style('assets/css/style.css') !!}
    {!! Html::style('assets/css/style-mango.css') !!}
    {!! Html::style('assets/css/vendors.css') !!}
    {!! Html::style('assets/css/themes/grey.css') !!}
    {!! Html::style('assets/css/style-responsive.css') !!}

    @yield('style')

</head>