<nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
    <div class="sidebar-collapse menu-scroll">
        <ul id="side-menu" class="nav">
            <li class="clock"><strong id="get-date"></strong>

                <div class="digital-clock">
                    <div id="getHours" class="get-time"></div>
                    <span>:</span>

                    <div id="getMinutes" class="get-time"></div>
                    <span>:</span>

                    <div id="getSeconds" class="get-time"></div>
                </div>
            </li>
            <li class="active"><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-laptop fa-fw"></i><span class="menu-title">Dashboard</span></a></li>
            <li class="sidebar-heading"><h4>Clients</h4></li>
            
            <li><a href="{!! url('admin/client') !!}"><i class="fa fa-user fa-fw"></i><span class="menu-title">Clients Listing</span></a></li>
            <li class="sidebar-heading"><h4>Billing</h4></li>
            
            <li><a href="{!! url('admin/invoice') !!}"><i class="fa fa-list fa-fw"></i><span class="menu-title">Invoices Listing</span></a></li>
            <li><a href="{!! url('admin/invoice/create') !!}"><i class="fa fa-plus fa-fw"></i><span class="menu-title">Add New Invoice</span></a></li>
            <li><a href="javascript:void(0);"><i class="fa fa-file fa-fw"></i><span class="menu-title">Receipts Listing</span></a></li>
            <li><a href="javascript:void(0);"><i class="fa fa-list-alt fa-fw"></i><span class="menu-title">Quotes Listing</span></a></li>
            <li><a href="javascript:void(0);"><i class="fa fa-plus fa-fw"></i><span class="menu-title">Add New Quote</span></a></li>
            
            <li class="sidebar-heading"><h4>Orders</h4></li>
            <li><a href="javascript:void(0);"><i class="fa fa-shopping-cart fa-fw"></i><span class="menu-title">Orders</span><span class="badge badge-blue">3</span></a></li>
            <li class="sidebar-heading"><h4>Services</h4></li>
            <li><a href="javascript:void(0);"><i class="fa fa-cog fa-fw"></i><span class="menu-title">Categories</span></a> </li>
            <li><a href="javascript:void(0);"><i class="fa fa-globe fa-fw"></i><span class="menu-title">Domains</span><span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                  <li><a href="javascript:void(0);">Single Domain Pricing List</a></li>
                  <li><a href="javascript:void(0);">Bulk Domain Pricing List</a></li>
                  <li><a href="javascript:void(0);">Domain Addons Pricing List</a></li>
              </ul>
          </li> 
          <li><a href="javascript:void(0);"><i class="fa fa-bullhorn fa-fw"></i><span class="menu-title">Client Schedule Notification</span></a></li> 
          <li><a href="javascript:void(0);"><i class="fa fa-info-circle fa-fw"></i><span class="menu-title">Services Enquiry Listing</span></a></li>
          
          
          <li class="sidebar-heading"><h4>Promotions</h4></li>
          <li><a href="javascript:void(0);"><i class="fa fa-cog fa-fw"></i><span class="menu-title">Global Discounts</span></a> </li>
          <li><a href="javascript:void(0);"><i class="fa fa-qrcode fa-fw"></i><span class="menu-title">Promo Codes</span></a></li>
          <li><a href="javascript:void(0);"><i class="fa fa-envelope fa-fw"></i><span class="menu-title">Newsletter Subscription</span></a> </li>
          
          <li class="sidebar-heading"><h4>Support</h4></li>
          <li><a href="javascript:void(0);"><i class="fa fa-wrench fa-fw"></i><span class="menu-title">Support Tickets/Service Enquiy/Ask For Quote Listing</span></a></li>
          
          <li class="sidebar-heading"><h4>Banners</h4></li>
          <li><a href="javascript:void(0);"><i class="fa fa-check-circle fa-fw"></i><span class="menu-title">Index Banners</span></a></li>
          
          <li class="sidebar-heading"><h4>CMS Pages</h4></li>
          <li><a href="javascript:void(0);"><i class="fa fa-check-circle fa-fw"></i><span class="menu-title">Blog</span></a></li>
          <li><a href="javascript:void(0);"><i class="fa fa-globe fa-fw"></i><span class="menu-title">Domains</span><span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                  <li><a href="javascript:void(0);">Domains Main Page</a></li>
                  <li><a href="javascript:void(0);">Single Domain Search</a></li>
                  <li><a href="javascript:void(0);">Bulk Domain Search</a></li>
                  <li><a href="javascript:void(0);">Single Domain Transfer</a></li>
                  <li><a href="javascript:void(0);">Bulk Domain Transfer</a></li>
              </ul>
          </li>
          
          <li class="sidebar-heading"><h4>Global Setup</h4></li>
          <li><a href="javascript:void(0);"><i class="fa fa-dollar fa-fw"></i><span class="menu-title">GST Rate Setup</span></a></li>
      </ul>
  </div>
</nav>