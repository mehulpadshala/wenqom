<!DOCTYPE html>
<html lang="en">
    @include('layouts.admin.partial.head')
    <body>
    	<div>
    		<a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
			@if(Auth::check())
	            @include('layouts.admin.partial.header')
	            @include('layouts.admin.partial.sidebar')
	        @endif
	        @yield('content')
	        @include('layouts.admin.partial.footer')
	    </div>
    </body>
</html>