@extends('layouts.admin.master')

@section('title', 'Login: Welcome to Web88 Administration Panel')

@section('content')
<body id="signin-page" class="animated bounceInDown">
    <div id="signin-page-content">
        
        {!! Form::open(array('action' => 'Auth\AuthController@postLogin', 'id' => 'login', 'class' => 'form')) !!}
            
            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />

            <div class="text-center"><a href="http://www.webqom.com/web88.html" target="_blank"><img src="{{imgUrl("login/logo_web88.jpg")}}" alt="Web88"></a></div>
            <h1 class="block-heading">Web88 Admin Login</h1>

            <p>Please enter your login details to access admin area.</p>
            
            @include('shared.validation')

            <div class="form-group">
                <div class="input-icon"><i class="fa fa-user"></i><input type="text" placeholder="Email" name="email" class="form-control"></div>
            </div>
            <div class="form-group">
                <div class="input-icon"><i class="fa fa-key"></i><input type="password" placeholder="Password" name="password" class="form-control"></div>
            </div>
            <div class="form-group">
                <div class="checkbox"><label><input type="checkbox" name="remember">&nbsp;
                    Remember me</label></div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Login&nbsp;<i class="fa fa-chevron-circle-right"></i></button>
                <a id="btn-forgot-pwd" href="forgot_password.html" class="mlm">Forgot your password?</a>
            </div>
            <hr>
            <a href="mailto:hock@webqom.com"><i class="fa fa-envelope"></i> hock@webqom.com</a>
            <a href="http://www.facebook.com/webqomtechnologies" class="pull-right" target="_blank"><i class="fa fa-facebook-square"></i> /webqomtechnologies</a><br/>
            <i class="fa fa-phone"></i>+(603) 2630 5562
            <span class="margin-top-5px text-12px pull-right">&copy; 2015 <a href="http://www.webqom.com" target="_blank">Webqom Technologies Sdn. Bhd.</a></span>
        
        {!! Form::close() !!}
    
    </div>
<body>
@stop