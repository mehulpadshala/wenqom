<!-- Display flash data -->
<div class="flash-message">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <i class="fa fa-check-circle fa-lg"></i>&nbsp;&nbsp;
            {!! session('success') !!}
        </div>
    @elseif(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <i class="fa fa-times-circle fa-lg"></i>&nbsp;&nbsp;
            {!! session('error') !!}
        </div>
    @endif
</div>
<!-- End: flash data -->