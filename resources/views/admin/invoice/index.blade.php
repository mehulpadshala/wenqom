@extends('layouts.admin.master')

@section('title', 'Invoice::Listing')

@section('style')

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>

{!! Html::style('assets/vendors/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('assets/vendors/bootstrap-datepicker/css/datepicker.css') !!}

{!! Html::style('assets/vendors/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css') !!}
{!! Html::style('assets/vendors/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('assets/vendors/bootstrap/css/bootstrap.min.css') !!}

<!--LOADING SCRIPTS FOR PAGE-->
{!! Html::style('assets/vendors/bootstrap-datepicker/css/datepicker.css') !!}
{!! Html::style('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') !!}

<!--Loading style vendors-->
{!! Html::style('assets/vendors/animate.css/animate.css') !!}
{!! Html::style('assets/vendors/jquery-pace/pace.css') !!}

<!--Loading style-->
{!! Html::style('assets/css/style.css') !!}  
{!! Html::style('assets/css/style.css') !!}
{!! Html::style('assets/css/style-mango.css') !!}
{!! Html::style('assets/css/vendors.css') !!}
{!! Html::style('assets/css/themes/grey.css') !!}
{!! Html::style('assets/css/style-responsive.css') !!}

<!-- webqom frontend style css -->
{!! Html::style('assets/css/style_webqom_front.css') !!}
{!! Html::style('assets/css/reset.css') !!}
{!! Html::style('assets/css/responsive-leyouts_webqom_front.css') !!}

@stop

@section('content')
<div id="page-wrapper">
    <div class="page-header-breadcrumb">
        <div class="page-heading hidden-xs">
            <h1 class="page-title">Invoices</h1>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url('admin/dashboard') }}">Dashboard</a>&nbsp; <i class="fa fa-angle-right"></i>&nbsp;</li>
            <li>Invoice &nbsp;<i class="fa fa-angle-right"></i>&nbsp;</li>
            <li class="active">Invoice - Listing</li>
        </ol>
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <h2>Invoices <i class="fa fa-angle-right"></i> Listing</h2>
                <div class="clearfix"></div>
            </div>
            <!-- end col-lg-12 -->
            <div class="col-lg-12">
              <div class="portlet">
                <div class="portlet-header">
                  @include('shared.flashMessage')
                  <div class="caption">Invoices Listing</div>
                  <p class="margin-top-10px"></p>
                  <a href="{{ url('admin/invoice/create') }}" class="btn btn-success">Add New Invoice &nbsp;<i class="fa fa-plus"></i></a>&nbsp;
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary">Delete</button>
                    <button type="button" data-toggle="dropdown" class="btn btn-red dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="#" data-target="#modal-delete-selected" data-toggle="modal">Delete selected item(s)</a></li>
                      <li class="divider"></li>
                      <li><a href="#" data-target="#modal-delete-all" data-toggle="modal">Delete all</a></li>
                  </ul>
              </div> 
              <a href="#" class="btn btn-blue">Export to CSV &nbsp;<i class="fa fa-share"></i></a>  
              <div class="tools"> <i class="fa fa-chevron-up"></i> </div>
              <!--Modal delete selected items start-->
              <div id="modal-delete-selected" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                      <h4 id="modal-login-label4" class="modal-title"><a href=""><i class="fa fa-exclamation-triangle"></i></a> Are you sure you want to delete the selected item(s)? </h4>
                  </div>
                  <div class="modal-body">
                      <p><strong>Invoice #:</strong> MY-7974188<br/>
                          <strong>Client Name:</strong> Hock Lim </p>
                          <div class="form-actions">
                            <div class="col-md-offset-4 col-md-8"> <a href="#" class="btn btn-red">Yes &nbsp;<i class="fa fa-check"></i></a>&nbsp; <a href="#" data-dismiss="modal" class="btn btn-green">No &nbsp;<i class="fa fa-times-circle"></i></a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal delete selected items end -->
        <!--Modal delete all items start-->
        <div id="modal-delete-all" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-login-label4" class="modal-title"><a href=""><i class="fa fa-exclamation-triangle"></i></a> Are you sure you want to delete all items? </h4>
              </div>
              <div class="modal-body">
                  <div class="form-actions">
                    <div class="col-md-offset-4 col-md-8"> <a href="#" class="btn btn-red">Yes &nbsp;<i class="fa fa-check"></i></a>&nbsp; <a href="#" data-dismiss="modal" class="btn btn-green">No &nbsp;<i class="fa fa-times-circle"></i></a> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal delete all items end -->
</div>
<div class="portlet-body">
  <div class="form-inline pull-left">
    <div class="form-group">
      <select name="select" class="form-control">
        <option>10</option>
        <option>20</option>
        <option>30</option>
        <option>50</option>
        <option selected="selected">100</option>
    </select>
    &nbsp;
    <label class="control-label">Records per page</label>
</div>
</div>
<div class="clearfix"></div>

<div class="table-responsive mtl">
    <table id="example1" class="table table-hover table-striped">
      <thead>
        <tr>
          <th width="1%"><input type="checkbox"/></th>
          <th>#</th>
          <th><a href="#sort by invoice">Invoice #</a></th>
          <th><a href="#sort by client id">Client ID</a></th>
          <th><a href="#sort by customer name">Client Name</a></th>
          <th><a href="#sort by invoice date">Invoice Date</a></th>
          <th><a href="#sort by due date">Due Date</a></th>
          <th><a href="#sort by total">Total</a></th>
          <th><a href="#sort by payment date">Payment Date</a></th>
          <th><a href="#sort by transaction id">Transaction ID</a></th>
          <th><a href="#sort by payment method">Payment Method</a></th>
          <th><a href="#sort by status">Status</a></th>
          <th>Action</th>
      </tr>
  </thead>
  <tbody>
    @foreach($invoices as $key => $invoice)
        <tr>
            <td><input type="checkbox" name="{{ $invoice->id }}"/></td>
            <td>{{ $invoice->id }}</td>
            <td>{{ $invoice->account_id }}</td>
            <td><a href="javascript:void(0);">{{ $invoice->account_id }}</a></td>
            <td><a href="javascript:void(0);">{{ $invoice->first_name .' '. $invoice->last_name}}</a></td>
            <td>16th Feb 2015</td>
            <td>16th Apr 2015</td>
            <td>RM 25,195.14</td>
            <td>16th Apr 2015</td>
            <td>4420911</td>
            <td>PayPal</td>
            <td><span class="label label-xs label-success">Paid</span></td>
            <td><a href="billing_invoice_edit.html" data-hover="tooltip" data-placement="top" title="Edit"><span class="label label-sm label-success"><i class="fa fa-pencil"></i></span></a> <a href="#" data-hover="tooltip" data-placement="top" title="Delete" data-target="#modal-delete-3" data-toggle="modal"><span class="label label-sm label-red"><i class="fa fa-trash-o"></i></span></a>
                <div id="modal-delete-3" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-login-label4" class="modal-title"><a href=""><i class="fa fa-exclamation-triangle"></i></a> Are you sure you want to delete this invoice? </h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <strong>Invoice #:</strong> MY-7974188 <br/>
                                    <strong>Client Name:</strong> Hock Lim 
                                </p>
                                <div class="form-actions">
                                    <div class="col-md-offset-4 col-md-8"> <a href="#" class="btn btn-red">Yes &nbsp;<i class="fa fa-check"></i></a>&nbsp; <a href="#" data-dismiss="modal" class="btn btn-green">No &nbsp;<i class="fa fa-times-circle"></i></a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
</tbody>
<tfoot>
    <tr>
      <td colspan="13"></td>
  </tr>
</tfoot>
</table>
<div class="tool-footer text-right">
  <p class="pull-left">Showing 1 to 10 of 57 entries</p>
  <ul class="pagination pagination mtm mbm">
    <li class="disabled"><a href="#">&laquo;</a></li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">&raquo;</a></li>
</ul>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!-- end portlet -->
</div>
<!-- end col-lg-12 -->
</div>
</div>
@stop

@section('script')
{!! Html::script('assets/vendors/metisMenu/jquery.metisMenu.js') !!}
{!! Html::script('assets/vendors/slimScroll/jquery.slimscroll.js') !!}
{!! Html::script('assets/vendors/jquery-cookie/jquery.cookie.js') !!}
{!! Html::script('assets/js/jquery.menu.js') !!}
{!! Html::script('assets/vendors/jquery-pace/pace.min.js') !!}

{!! Html::script('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}
{!! Html::script('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') !!}
{!! Html::script('assets/vendors/moment/moment.js') !!}
{!! Html::script('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
{!! Html::script('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}
{!! Html::script('assets/vendors/bootstrap-clockface/js/clockface.js') !!}
{!! Html::script('assets/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js') !!}
{!! Html::script('assets/vendors/bootstrap-switch/js/bootstrap-switch.min.js') !!}
{!! Html::script('assets/vendors/jquery-maskedinput/jquery-maskedinput.js') !!}
{!! Html::script('assets/js/form-components.js') !!}

{!! Html::script('assets/vendors/tinymce/js/tinymce/tinymce.min.js') !!}
{!! Html::script('assets/vendors/ckeditor/ckeditor.js') !!}
{!! Html::script('assets/js/ui-tabs-accordions-navs.js') !!}

{!! Html::script('assets/js/main.js') !!}
{!! Html::script('assets/js/holder.js') !!}
@stop              