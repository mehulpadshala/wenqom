@extends('layouts.admin.master')

@section('title', 'Client::Listring')

@section('style')

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>

{!! Html::style('assets/vendors/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('assets/vendors/bootstrap-datepicker/css/datepicker.css') !!}

{!! Html::style('assets/vendors/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css') !!}
{!! Html::style('assets/vendors/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('assets/vendors/bootstrap/css/bootstrap.min.css') !!}
    
<!--LOADING SCRIPTS FOR PAGE-->
{!! Html::style('assets/vendors/bootstrap-datepicker/css/datepicker.css') !!}
{!! Html::style('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') !!}
    
<!--Loading style vendors-->
{!! Html::style('assets/vendors/animate.css/animate.css') !!}
{!! Html::style('assets/vendors/jquery-pace/pace.css') !!}

<!--Loading style-->
{!! Html::style('assets/css/style.css') !!}  
{!! Html::style('assets/css/style.css') !!}
{!! Html::style('assets/css/style-mango.css') !!}
{!! Html::style('assets/css/vendors.css') !!}
{!! Html::style('assets/css/themes/grey.css') !!}
{!! Html::style('assets/css/style-responsive.css') !!}

<!-- webqom frontend style css -->
{!! Html::style('assets/css/style_webqom_front.css') !!}
{!! Html::style('assets/css/reset.css') !!}
{!! Html::style('assets/css/responsive-leyouts_webqom_front.css') !!}

@stop

@section('content')
<div id="page-wrapper">
    <div class="page-header-breadcrumb">
        <div class="page-heading hidden-xs">
            <h1 class="page-title">Clients</h1>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url('admin/dashboard') }}">Dashboard</a>&nbsp; <i class="fa fa-angle-right"></i>&nbsp;</li>
            <li>Clients &nbsp;<i class="fa fa-angle-right"></i>&nbsp;</li>
            <li class="active">Clients - Listing</li>
        </ol>
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet">
                    <div class="portlet-header">
                        <div class="caption">Clients Listing</div>
                        <p class="margin-top-10px"></p>
                        <a href="#" class="btn btn-success" data-target="#modal-add-client" data-toggle="modal">Add New Client &nbsp;<i class="fa fa-plus"></i></a>&nbsp;
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Delete</button>
                            <button type="button" data-toggle="dropdown" class="btn btn-red dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="#" data-target="#modal-delete-selected" data-toggle="modal">Delete selected item(s)</a></li>
                                <li class="divider"></li>
                                <li><a href="#" data-target="#modal-delete-all" data-toggle="modal">Delete all</a></li>
                            </ul>
                        </div>&nbsp;
                        <a href="#" class="btn btn-blue">Export to CSV &nbsp;<i class="fa fa-share"></i></a>  
                        <div class="tools"> <i class="fa fa-chevron-up"></i> 
                        </div>
                        <div id="modal-add-client" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
                            <div class="modal-dialog modal-wide-width">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                        <h4 id="modal-login-label3" class="modal-title">Add New Client</h4>
                                    </div>
                                    <div class="modal-body">
                                        @include('shared.validation')
                                        <div class="form">
                                            {!! Form::open(['url' => 'admin/client', 'name' => 'client_form', 'class' => 'form-horizontal']) !!}
                                                <h5 class="block-heading">Client Account Information</h5>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Status <span class="text-red">*</span></label>
                                                    <div class="col-md-6">
                                                        <div data-on="success" data-off="primary" class="make-switch">
                                                            <input type="checkbox" checked="checked" name="status"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Account Type <span class="text-red">* </span></label>
                                                    <div class="col-md-6">
                                                        <div class="radio-list">
                                                            <label class="radio-inline">
                                                                <input id="optionsRadios4" type="radio" name="account_type" value="1"/>&nbsp; Business Account
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input id="optionsRadios5" type="radio" name="account_type" value="2"/>&nbsp; Individual Account
                                                            </label>
                                                        </div>
                                                    </div>
                                                    note to programmer: if it is "Business Account", the ID auto-generated begin with B followed with the next available sequential number and the country code. Example B-00012-SG, SG is the country code for Singapore and if for India, it will be IN etc. 
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">First Name <span class="text-red">*</span>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="first_name" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Last Name <span class="text-red">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="last_name" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Company <span class="text-red">* </span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="company" class="form-control" placeholder="">
                                                        <div class="text-11px text-red">(* Mandatory for Business Account)
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Email <span class="text-red">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="email" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Phone <span class="text-red">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="phone" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Mobile Phone <span class="text-red">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="cell_phone" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Address <span class="text-red">* </span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="address" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Address 2 </label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="address_2" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">City <span class="text-red">* </span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="city" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Postal Code <span class="text-red">* </span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="postal_code" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">State <span class="text-red">* </span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="state"  class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Country <span class="text-red">* </span></label>
                                                    <div class="col-md-6">
                                                        <select name="country" class="form-control">
                                                            <option>-- Please select --</option>
                                                            <option data-calling-code="93" data-eu-tax="unknown" value="AF">Afghanistan</option>
                                                            <option data-calling-code="358" data-eu-tax="unknown" value="AX">Åland Islands</option>
                                                            <option data-calling-code="355" data-eu-tax="unknown" value="AL">Albania</option>
                                                            <option data-calling-code="213" data-eu-tax="unknown" value="DZ">Algeria</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="AS">American Samoa</option>
                                                            <option data-calling-code="376" data-eu-tax="unknown" value="AD">Andorra</option>
                                                            <option data-calling-code="244" data-eu-tax="unknown" value="AO">Angola</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="AI">Anguilla</option>
                                                            <option data-calling-code="672" data-eu-tax="unknown" value="AQ">Antarctica</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="AG">Antigua and Barbuda</option>
                                                            <option data-calling-code="54" data-eu-tax="unknown" value="AR">Argentina</option>
                                                            <option data-calling-code="374" data-eu-tax="unknown" value="AM">Armenia</option>
                                                            <option data-calling-code="297" data-eu-tax="unknown" value="AW">Aruba</option>
                                                            <option data-calling-code="61" data-eu-tax="unknown" value="AU">Australia</option>
                                                            <option data-calling-code="43" data-eu-tax="yes" value="AT">Austria</option>
                                                            <option data-calling-code="994" data-eu-tax="unknown" value="AZ">Azerbaijan</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="BS">Bahamas</option>
                                                            <option data-calling-code="973" data-eu-tax="unknown" value="BH">Bahrain</option>
                                                            <option data-calling-code="880" data-eu-tax="unknown" value="BD">Bangladesh</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="BB">Barbados</option>
                                                            <option data-calling-code="375" data-eu-tax="unknown" value="BY">Belarus</option>
                                                            <option data-calling-code="32" data-eu-tax="yes" value="BE">Belgium</option>
                                                            <option data-calling-code="501" data-eu-tax="unknown" value="BZ">Belize</option>
                                                            <option data-calling-code="229" data-eu-tax="unknown" value="BJ">Benin</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="BM">Bermuda</option>
                                                            <option data-calling-code="975" data-eu-tax="unknown" value="BT">Bhutan</option>
                                                            <option data-calling-code="591" data-eu-tax="unknown" value="BO">Bolivia, Plurinational State of</option>
                                                            <option data-calling-code="387" data-eu-tax="unknown" value="BA">Bosnia and Herzegovina</option>
                                                            <option data-calling-code="267" data-eu-tax="unknown" value="BW">Botswana</option>
                                                            <option data-calling-code="55" data-eu-tax="unknown" value="BR">Brazil</option>
                                                            <option data-calling-code="246" data-eu-tax="unknown" value="IO">British Indian Ocean Territory</option>
                                                            <option data-calling-code="673" data-eu-tax="unknown" value="BN">Brunei Darussalam</option>
                                                            <option data-calling-code="359" data-eu-tax="yes" value="BG">Bulgaria</option>
                                                            <option data-calling-code="226" data-eu-tax="unknown" value="BF">Burkina Faso</option>
                                                            <option data-calling-code="257" data-eu-tax="unknown" value="BI">Burundi</option>
                                                            <option data-calling-code="855" data-eu-tax="unknown" value="KH">Cambodia</option>
                                                            <option data-calling-code="237" data-eu-tax="unknown" value="CM">Cameroon</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="CA">Canada</option>
                                                            <option data-calling-code="238" data-eu-tax="unknown" value="CV">Cape Verde</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="KY">Cayman Islands</option>
                                                            <option data-calling-code="236" data-eu-tax="unknown" value="CF">Central African Republic</option>
                                                            <option data-calling-code="235" data-eu-tax="unknown" value="TD">Chad</option>
                                                            <option data-calling-code="56" data-eu-tax="unknown" value="CL">Chile</option>
                                                            <option data-calling-code="86" data-eu-tax="unknown" value="CN">China</option>
                                                            <option data-calling-code="61" data-eu-tax="unknown" value="CX">Christmas Island</option>
                                                            <option data-calling-code="61" data-eu-tax="unknown" value="CC">Cocos (Keeling) Islands</option>
                                                            <option data-calling-code="57" data-eu-tax="unknown" value="CO">Colombia</option>
                                                            <option data-calling-code="269" data-eu-tax="unknown" value="KM">Comoros</option>
                                                            <option data-calling-code="242" data-eu-tax="unknown" value="CG">Congo</option>
                                                            <option data-calling-code="243" data-eu-tax="unknown" value="CD">Congo, the Democratic Republic of the</option>
                                                            <option data-calling-code="682" data-eu-tax="unknown" value="CK">Cook Islands</option>
                                                            <option data-calling-code="506" data-eu-tax="unknown" value="CR">Costa Rica</option>
                                                            <option data-calling-code="225" data-eu-tax="unknown" value="CI">Côte d'Ivoire</option>
                                                            <option data-calling-code="385" data-eu-tax="yes" value="HR">Croatia</option>
                                                            <option data-calling-code="53" data-eu-tax="unknown" value="CU">Cuba</option>
                                                            <option data-calling-code="357" data-eu-tax="yes" value="CY">Cyprus</option>
                                                            <option data-calling-code="420" data-eu-tax="yes" value="CZ">Czech Republic</option>
                                                            <option data-calling-code="45" data-eu-tax="yes" value="DK">Denmark</option>
                                                            <option data-calling-code="253" data-eu-tax="unknown" value="DJ">Djibouti</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="DM">Dominica</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="DO">Dominican Republic</option>
                                                            <option data-calling-code="593" data-eu-tax="unknown" value="EC">Ecuador</option>
                                                            <option data-calling-code="20" data-eu-tax="unknown" value="EG">Egypt</option>
                                                            <option data-calling-code="503" data-eu-tax="unknown" value="SV">El Salvador</option>
                                                            <option data-calling-code="240" data-eu-tax="unknown" value="GQ">Equatorial Guinea</option>
                                                            <option data-calling-code="291" data-eu-tax="unknown" value="ER">Eritrea</option>
                                                            <option data-calling-code="372" data-eu-tax="yes" value="EE">Estonia</option>
                                                            <option data-calling-code="251" data-eu-tax="unknown" value="ET">Ethiopia</option>
                                                            <option data-calling-code="500" data-eu-tax="unknown" value="FK">Falkland Islands (Malvinas)</option>
                                                            <option data-calling-code="298" data-eu-tax="unknown" value="FO">Faroe Islands</option>
                                                            <option data-calling-code="679" data-eu-tax="unknown" value="FJ">Fiji</option>
                                                            <option data-calling-code="358" data-eu-tax="yes" value="FI">Finland</option>
                                                            <option data-calling-code="33" data-eu-tax="yes" value="FR">France</option>
                                                            <option data-calling-code="594" data-eu-tax="unknown" value="GF">French Guiana</option>
                                                            <option data-calling-code="689" data-eu-tax="unknown" value="PF">French Polynesia</option>
                                                            <option data-calling-code="262" data-eu-tax="unknown" value="TF">French Southern Territories</option>
                                                            <option data-calling-code="241" data-eu-tax="unknown" value="GA">Gabon</option>
                                                            <option data-calling-code="220" data-eu-tax="unknown" value="GM">Gambia</option>
                                                            <option data-calling-code="995" data-eu-tax="unknown" value="GE">Georgia</option>
                                                            <option data-calling-code="49" data-eu-tax="yes" value="DE">Germany</option>
                                                            <option data-calling-code="233" data-eu-tax="unknown" value="GH">Ghana</option>
                                                            <option data-calling-code="350" data-eu-tax="unknown" value="GI">Gibraltar</option>
                                                            <option data-calling-code="30" data-eu-tax="yes" value="GR">Greece</option>
                                                            <option data-calling-code="299" data-eu-tax="unknown" value="GL">Greenland</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="GD">Grenada</option>
                                                            <option data-calling-code="590" data-eu-tax="unknown" value="GP">Guadeloupe</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="GU">Guam</option>
                                                            <option data-calling-code="502" data-eu-tax="unknown" value="GT">Guatemala</option>
                                                            <option data-calling-code="44" data-eu-tax="unknown" value="GG">Guernsey</option>
                                                            <option data-calling-code="224" data-eu-tax="unknown" value="GN">Guinea</option>
                                                            <option data-calling-code="245" data-eu-tax="unknown" value="GW">Guinea-Bissau</option>
                                                            <option data-calling-code="592" data-eu-tax="unknown" value="GY">Guyana</option>
                                                            <option data-calling-code="509" data-eu-tax="unknown" value="HT">Haiti</option>
                                                            <option data-calling-code="3906" data-eu-tax="unknown" value="VA">Holy See (Vatican City State)</option>
                                                            <option data-calling-code="504" data-eu-tax="unknown" value="HN">Honduras</option>
                                                            <option data-calling-code="852" data-eu-tax="unknown" value="HK">Hong Kong</option>
                                                            <option data-calling-code="36" data-eu-tax="yes" value="HU">Hungary</option>
                                                            <option data-calling-code="354" data-eu-tax="unknown" value="IS">Iceland</option>
                                                            <option data-calling-code="91" data-eu-tax="unknown" value="IN">India</option>
                                                            <option data-calling-code="62" data-eu-tax="unknown" value="ID">Indonesia</option>
                                                            <option data-calling-code="98" data-eu-tax="unknown" value="IR">Iran, Islamic Republic of</option>
                                                            <option data-calling-code="964" data-eu-tax="unknown" value="IQ">Iraq</option>
                                                            <option data-calling-code="353" data-eu-tax="yes" value="IE">Ireland</option>
                                                            <option data-calling-code="44" data-eu-tax="yes" value="IM">Isle of Man</option>
                                                            <option data-calling-code="972" data-eu-tax="unknown" value="IL">Israel</option>
                                                            <option data-calling-code="39" data-eu-tax="yes" value="IT">Italy</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="JM">Jamaica</option>
                                                            <option data-calling-code="81" data-eu-tax="unknown" value="JP">Japan</option>
                                                            <option data-calling-code="44" data-eu-tax="unknown" value="JE">Jersey</option>
                                                            <option data-calling-code="962" data-eu-tax="unknown" value="JO">Jordan</option>
                                                            <option data-calling-code="7" data-eu-tax="unknown" value="KZ">Kazakhstan</option>
                                                            <option data-calling-code="254" data-eu-tax="unknown" value="KE">Kenya</option>
                                                            <option data-calling-code="686" data-eu-tax="unknown" value="KI">Kiribati</option>
                                                            <option data-calling-code="850" data-eu-tax="unknown" value="KP">Korea, Democratic People's Republic of</option>
                                                            <option data-calling-code="82" data-eu-tax="unknown" value="KR">Korea, Republic of</option>
                                                            <option data-calling-code="965" data-eu-tax="unknown" value="KW">Kuwait</option>
                                                            <option data-calling-code="996" data-eu-tax="unknown" value="KG">Kyrgyzstan</option>
                                                            <option data-calling-code="856" data-eu-tax="unknown" value="LA">Lao People's Democratic Republic</option>
                                                            <option data-calling-code="371" data-eu-tax="yes" value="LV">Latvia</option>
                                                            <option data-calling-code="961" data-eu-tax="unknown" value="LB">Lebanon</option>
                                                            <option data-calling-code="266" data-eu-tax="unknown" value="LS">Lesotho</option>
                                                            <option data-calling-code="231" data-eu-tax="unknown" value="LR">Liberia</option>
                                                            <option data-calling-code="218" data-eu-tax="unknown" value="LY">Libyan Arab Jamahiriya</option>
                                                            <option data-calling-code="423" data-eu-tax="unknown" value="LI">Liechtenstein</option>
                                                            <option data-calling-code="370" data-eu-tax="yes" value="LT">Lithuania</option>
                                                            <option data-calling-code="352" data-eu-tax="yes" value="LU">Luxembourg</option>
                                                            <option data-calling-code="853" data-eu-tax="unknown" value="MO">Macao</option>
                                                            <option data-calling-code="389" data-eu-tax="unknown" value="MK">Macedonia, the former Yugoslav Republic of</option>
                                                            <option data-calling-code="261" data-eu-tax="unknown" value="MG">Madagascar</option>
                                                            <option data-calling-code="265" data-eu-tax="unknown" value="MW">Malawi</option>
                                                            <option data-calling-code="60" data-eu-tax="unknown" value="MY" selected="selected">Malaysia</option>
                                                            <option data-calling-code="960" data-eu-tax="unknown" value="MV">Maldives</option>
                                                            <option data-calling-code="223" data-eu-tax="unknown" value="ML">Mali</option>
                                                            <option data-calling-code="356" data-eu-tax="yes" value="MT">Malta</option>
                                                            <option data-calling-code="692" data-eu-tax="unknown" value="MH">Marshall Islands</option>
                                                            <option data-calling-code="596" data-eu-tax="unknown" value="MQ">Martinique</option>
                                                            <option data-calling-code="222" data-eu-tax="unknown" value="MR">Mauritania</option>
                                                            <option data-calling-code="230" data-eu-tax="unknown" value="MU">Mauritius</option>
                                                            <option data-calling-code="262" data-eu-tax="unknown" value="YT">Mayotte</option>
                                                            <option data-calling-code="52" data-eu-tax="unknown" value="MX">Mexico</option>
                                                            <option data-calling-code="691" data-eu-tax="unknown" value="FM">Micronesia, Federated States of</option>
                                                            <option data-calling-code="373" data-eu-tax="unknown" value="MD">Moldova, Republic of</option>
                                                            <option data-calling-code="377" data-eu-tax="yes" value="MC">Monaco</option>
                                                            <option data-calling-code="976" data-eu-tax="unknown" value="MN">Mongolia</option>
                                                            <option data-calling-code="382" data-eu-tax="unknown" value="ME">Montenegro</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="MS">Montserrat</option>
                                                            <option data-calling-code="212" data-eu-tax="unknown" value="MA">Morocco</option>
                                                            <option data-calling-code="258" data-eu-tax="unknown" value="MZ">Mozambique</option>
                                                            <option data-calling-code="94" data-eu-tax="unknown" value="MM">Myanmar</option>
                                                            <option data-calling-code="264" data-eu-tax="unknown" value="NA">Namibia</option>
                                                            <option data-calling-code="674" data-eu-tax="unknown" value="NR">Nauru</option>
                                                            <option data-calling-code="977" data-eu-tax="unknown" value="NP">Nepal</option>
                                                            <option data-calling-code="31" data-eu-tax="yes" value="NL">Netherlands</option>
                                                            <option data-calling-code="599" data-eu-tax="unknown" value="AN">Netherlands Antilles</option>
                                                            <option data-calling-code="687" data-eu-tax="unknown" value="NC">New Caledonia</option>
                                                            <option data-calling-code="64" data-eu-tax="unknown" value="NZ">New Zealand</option>
                                                            <option data-calling-code="505" data-eu-tax="unknown" value="NI">Nicaragua</option>
                                                            <option data-calling-code="227" data-eu-tax="unknown" value="NE">Niger</option>
                                                            <option data-calling-code="234" data-eu-tax="unknown" value="NG">Nigeria</option>
                                                            <option data-calling-code="683" data-eu-tax="unknown" value="NU">Niue</option>
                                                            <option data-calling-code="672" data-eu-tax="unknown" value="NF">Norfolk Island</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="MP">Northern Mariana Islands</option>
                                                            <option data-calling-code="47" data-eu-tax="unknown" value="NO">Norway</option>
                                                            <option data-calling-code="968" data-eu-tax="unknown" value="OM">Oman</option>
                                                            <option data-calling-code="92" data-eu-tax="unknown" value="PK">Pakistan</option>
                                                            <option data-calling-code="680" data-eu-tax="unknown" value="PW">Palau</option>
                                                            <option data-calling-code="970" data-eu-tax="unknown" value="PS">Palestinian Territory, Occupied</option>
                                                            <option data-calling-code="507" data-eu-tax="unknown" value="PA">Panama</option>
                                                            <option data-calling-code="675" data-eu-tax="unknown" value="PG">Papua New Guinea</option>
                                                            <option data-calling-code="595" data-eu-tax="unknown" value="PY">Paraguay</option>
                                                            <option data-calling-code="51" data-eu-tax="unknown" value="PE">Peru</option>
                                                            <option data-calling-code="63" data-eu-tax="unknown" value="PH">Philippines</option>
                                                            <option data-calling-code="649" data-eu-tax="unknown" value="PN">Pitcairn</option>
                                                            <option data-calling-code="48" data-eu-tax="yes" value="PL">Poland</option>
                                                            <option data-calling-code="351" data-eu-tax="yes" value="PT">Portugal</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="PR">Puerto Rico</option>
                                                            <option data-calling-code="974" data-eu-tax="unknown" value="QA">Qatar</option>
                                                            <option data-calling-code="262" data-eu-tax="unknown" value="RE">Réunion</option>
                                                            <option data-calling-code="40" data-eu-tax="yes" value="RO">Romania</option>
                                                            <option data-calling-code="7" data-eu-tax="unknown" value="RU">Russian Federation</option>
                                                            <option data-calling-code="250" data-eu-tax="unknown" value="RW">Rwanda</option>
                                                            <option data-calling-code="590" data-eu-tax="unknown" value="BL">Saint Barthélemy</option>
                                                            <option data-calling-code="290" data-eu-tax="unknown" value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="KN">Saint Kitts and Nevis</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="LC">Saint Lucia</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="MF">Saint Martin (French part)</option>
                                                            <option data-calling-code="508" data-eu-tax="unknown" value="PM">Saint Pierre and Miquelon</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="VC">Saint Vincent and the Grenadines</option>
                                                            <option data-calling-code="685" data-eu-tax="unknown" value="WS">Samoa</option>
                                                            <option data-calling-code="378" data-eu-tax="unknown" value="SM">San Marino</option>
                                                            <option data-calling-code="239" data-eu-tax="unknown" value="ST">Sao Tome and Principe</option>
                                                            <option data-calling-code="966" data-eu-tax="unknown" value="SA">Saudi Arabia</option>
                                                            <option data-calling-code="221" data-eu-tax="unknown" value="SN">Senegal</option>
                                                            <option data-calling-code="382" data-eu-tax="unknown" value="RS">Serbia</option>
                                                            <option data-calling-code="248" data-eu-tax="unknown" value="SC">Seychelles</option>
                                                            <option data-calling-code="232" data-eu-tax="unknown" value="SL">Sierra Leone</option>
                                                            <option data-calling-code="65" data-eu-tax="unknown" value="SG">Singapore</option>
                                                            <option data-calling-code="421" data-eu-tax="yes" value="SK">Slovakia</option>
                                                            <option data-calling-code="386" data-eu-tax="yes" value="SI">Slovenia</option>
                                                            <option data-calling-code="677" data-eu-tax="unknown" value="SB">Solomon Islands</option>
                                                            <option data-calling-code="252" data-eu-tax="unknown" value="SO">Somalia</option>
                                                            <option data-calling-code="27" data-eu-tax="unknown" value="ZA">South Africa</option>
                                                            <option data-calling-code="34" data-eu-tax="yes" value="ES">Spain</option>
                                                            <option data-calling-code="94" data-eu-tax="unknown" value="LK">Sri Lanka</option>
                                                            <option data-calling-code="249" data-eu-tax="unknown" value="SD">Sudan</option>
                                                            <option data-calling-code="597" data-eu-tax="unknown" value="SR">Suriname</option>
                                                            <option data-calling-code="" data-eu-tax="unknown" value="SJ">Svalbard and Jan Mayen</option>
                                                            <option data-calling-code="268" data-eu-tax="unknown" value="SZ">Swaziland</option>
                                                            <option data-calling-code="46" data-eu-tax="yes" value="SE">Sweden</option>
                                                            <option data-calling-code="41" data-eu-tax="unknown" value="CH">Switzerland</option>
                                                            <option data-calling-code="963" data-eu-tax="unknown" value="SY">Syrian Arab Republic</option>
                                                            <option data-calling-code="886" data-eu-tax="unknown" value="TW">Taiwan</option>
                                                            <option data-calling-code="992" data-eu-tax="unknown" value="TJ">Tajikistan</option>
                                                            <option data-calling-code="255" data-eu-tax="unknown" value="TZ">Tanzania, United Republic of</option>
                                                            <option data-calling-code="66" data-eu-tax="unknown" value="TH">Thailand</option>
                                                            <option data-calling-code="670" data-eu-tax="unknown" value="TL">Timor-Leste</option>
                                                            <option data-calling-code="228" data-eu-tax="unknown" value="TG">Togo</option>
                                                            <option data-calling-code="690" data-eu-tax="unknown" value="TK">Tokelau</option>
                                                            <option data-calling-code="676" data-eu-tax="unknown" value="TO">Tonga</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="TT">Trinidad and Tobago</option>
                                                            <option data-calling-code="216" data-eu-tax="unknown" value="TN">Tunisia</option>
                                                            <option data-calling-code="90" data-eu-tax="unknown" value="TR">Turkey</option>
                                                            <option data-calling-code="993" data-eu-tax="unknown" value="TM">Turkmenistan</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="TC">Turks and Caicos Islands</option>
                                                            <option data-calling-code="688" data-eu-tax="unknown" value="TV">Tuvalu</option>
                                                            <option data-calling-code="256" data-eu-tax="unknown" value="UG">Uganda</option>
                                                            <option data-calling-code="380" data-eu-tax="unknown" value="UA">Ukraine</option>
                                                            <option data-calling-code="971" data-eu-tax="unknown" value="AE">United Arab Emirates</option>
                                                            <option data-calling-code="44" data-eu-tax="yes" value="GB">United Kingdom</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="US">United States</option>
                                                            <option data-calling-code="598" data-eu-tax="unknown" value="UY">Uruguay</option>
                                                            <option data-calling-code="998" data-eu-tax="unknown" value="UZ">Uzbekistan</option>
                                                            <option data-calling-code="678" data-eu-tax="unknown" value="VU">Vanuatu</option>
                                                            <option data-calling-code="58" data-eu-tax="unknown" value="VE">Venezuela, Bolivarian Republic of</option>
                                                            <option data-calling-code="84" data-eu-tax="unknown" value="VN">Viet Nam</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="VG">Virgin Islands, British</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="VI">Virgin Islands, U.S.</option>
                                                            <option data-calling-code="681" data-eu-tax="unknown" value="WF">Wallis and Futuna</option>
                                                            <option data-calling-code="" data-eu-tax="unknown" value="EH">Western Sahara</option>
                                                            <option data-calling-code="967" data-eu-tax="unknown" value="YE">Yemen</option>
                                                            <option data-calling-code="260" data-eu-tax="unknown" value="ZM">Zambia</option>
                                                            <option data-calling-code="263" data-eu-tax="unknown" value="ZW">Zimbabwe</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="label" class="control-label col-md-4">Password <span class="text-red">*</span></label>
                                                    <div class="col-md-6">
                                                        <div class="input-icon"><i class="fa fa-key"></i>
                                                            <input id="label" name="password" type="password" placeholder="Password" class="form-control"/>
                                                            <span class="text-10px">(Password length should be between 6-12 characters)</span> 
                                                        </div>
                                                        <p><a href="#">Generate Password</a></p>
                                                        note to prorgrammer: after click "generate password", then only display the below field and randomly generate 100% strong password in the below field.
                                                        <input type="text" name="generate_password"  value="-^L^wuLQ9{S\" class="form-control"/>
                                                        <div class="col-md-offset-4 col-md-7 margin-top-10px"> 
                                                            <a href="#" class="btn btn-red">Ok &nbsp;<i class="fa fa-check"></i></a>&nbsp; <a href="#" data-dismiss="modal" class="btn btn-green">Cancel &nbsp;<i class="glyphicon glyphicon-ban-circle"></i></a> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="label" class="control-label col-md-4"></label>
                                                    <div class="col-md-6">
                                                        <div data-hover="tooltip" data-placement="top" title="100% Strong" class="progress progress-striped active">
                                                            <div role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-success"><span class="sr-only">100% Strong</span><span class="progress-completed">100% Strong</span>
                                                            </div>
                                                        </div>
                                                        <div data-hover="tooltip" data-placement="top" title="60% Moderate" class="progress progress-striped active">
                                                            <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" class="progress-bar progress-bar-warning"><span class="sr-only">60% Moderate</span><span class="progress-completed">60% Moderate</span></div>
                                                        </div>
                                                        <div data-hover="tooltip" data-placement="top" title="10% Weak" class="progress progress-striped active">
                                                            <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 10%;" class="progress-bar progress-bar-danger"><span class="sr-only">10% Weak</span><span class="progress-completed">10% Weak</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="label" class="control-label col-md-4">Confirm Password <span class="text-red">*</span></label>
                                                    <div class="col-md-6">
                                                        <div class="input-icon"><i class="fa fa-key"></i>
                                                            <input id="label" name="confirm_password" type="password" placeholder="Confirm Password" class="form-control"/>
                                                            <span class="text-10px">(Password length should be between 6-12 characters)</span> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Newsletter Subscription</label>
                                                    <div class="col-md-6">
                                                        <div class="margin-top-10px">
                                                            <input type="checkbox" name="news_letter_subscription" checked="checked">
                                                            Yes! I would like to subscribe to your newsletter. \
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="md-margin clearfix"></div>
                                                <h5 class="block-heading">Billing Address</h5>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"></label>
                                                    <div class="col-md-6">
                                                        <input type="checkbox" name="same_as_billing_address" checked="checked">My Billing address is the same as account information above. 
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                note to programmer: when checked the above, please auto fill in the information below.
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">First Name</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_first_name" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Last Name</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_last_name" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Company</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_company" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Email </label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_email" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Phone </label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_phone" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Mobile Phone </label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_cell_phone" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Address </label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_address" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Address2 </label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_address_2" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">City</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_city" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Postal Code</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="billing_postal_code" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">State</label>
                                                    <div class="col-md-6">
                                                      <input type="text" name="billing_state" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="col-md-4 control-label">Country</label>
                                                    <div class="col-md-6">
                                                        <select name="billing_country" class="form-control">
                                                            <option>-- Please select --</option>
                                                            <option data-calling-code="93" data-eu-tax="unknown" value="AF">Afghanistan</option>
                                                            <option data-calling-code="358" data-eu-tax="unknown" value="AX">Åland Islands</option>
                                                            <option data-calling-code="355" data-eu-tax="unknown" value="AL">Albania</option>
                                                            <option data-calling-code="213" data-eu-tax="unknown" value="DZ">Algeria</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="AS">American Samoa</option>
                                                            <option data-calling-code="376" data-eu-tax="unknown" value="AD">Andorra</option>
                                                            <option data-calling-code="244" data-eu-tax="unknown" value="AO">Angola</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="AI">Anguilla</option>
                                                            <option data-calling-code="672" data-eu-tax="unknown" value="AQ">Antarctica</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="AG">Antigua and Barbuda</option>
                                                            <option data-calling-code="54" data-eu-tax="unknown" value="AR">Argentina</option>
                                                            <option data-calling-code="374" data-eu-tax="unknown" value="AM">Armenia</option>
                                                            <option data-calling-code="297" data-eu-tax="unknown" value="AW">Aruba</option>
                                                            <option data-calling-code="61" data-eu-tax="unknown" value="AU">Australia</option>
                                                            <option data-calling-code="43" data-eu-tax="yes" value="AT">Austria</option>
                                                            <option data-calling-code="994" data-eu-tax="unknown" value="AZ">Azerbaijan</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="BS">Bahamas</option>
                                                            <option data-calling-code="973" data-eu-tax="unknown" value="BH">Bahrain</option>
                                                            <option data-calling-code="880" data-eu-tax="unknown" value="BD">Bangladesh</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="BB">Barbados</option>
                                                            <option data-calling-code="375" data-eu-tax="unknown" value="BY">Belarus</option>
                                                            <option data-calling-code="32" data-eu-tax="yes" value="BE">Belgium</option>
                                                            <option data-calling-code="501" data-eu-tax="unknown" value="BZ">Belize</option>
                                                            <option data-calling-code="229" data-eu-tax="unknown" value="BJ">Benin</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="BM">Bermuda</option>
                                                            <option data-calling-code="975" data-eu-tax="unknown" value="BT">Bhutan</option>
                                                            <option data-calling-code="591" data-eu-tax="unknown" value="BO">Bolivia, Plurinational State of</option>
                                                            <option data-calling-code="387" data-eu-tax="unknown" value="BA">Bosnia and Herzegovina</option>
                                                            <option data-calling-code="267" data-eu-tax="unknown" value="BW">Botswana</option>
                                                            <option data-calling-code="55" data-eu-tax="unknown" value="BR">Brazil</option>
                                                            <option data-calling-code="246" data-eu-tax="unknown" value="IO">British Indian Ocean Territory</option>
                                                            <option data-calling-code="673" data-eu-tax="unknown" value="BN">Brunei Darussalam</option>
                                                            <option data-calling-code="359" data-eu-tax="yes" value="BG">Bulgaria</option>
                                                            <option data-calling-code="226" data-eu-tax="unknown" value="BF">Burkina Faso</option>
                                                            <option data-calling-code="257" data-eu-tax="unknown" value="BI">Burundi</option>
                                                            <option data-calling-code="855" data-eu-tax="unknown" value="KH">Cambodia</option>
                                                            <option data-calling-code="237" data-eu-tax="unknown" value="CM">Cameroon</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="CA">Canada</option>
                                                            <option data-calling-code="238" data-eu-tax="unknown" value="CV">Cape Verde</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="KY">Cayman Islands</option>
                                                            <option data-calling-code="236" data-eu-tax="unknown" value="CF">Central African Republic</option>
                                                            <option data-calling-code="235" data-eu-tax="unknown" value="TD">Chad</option>
                                                            <option data-calling-code="56" data-eu-tax="unknown" value="CL">Chile</option>
                                                            <option data-calling-code="86" data-eu-tax="unknown" value="CN">China</option>
                                                            <option data-calling-code="61" data-eu-tax="unknown" value="CX">Christmas Island</option>
                                                            <option data-calling-code="61" data-eu-tax="unknown" value="CC">Cocos (Keeling) Islands</option>
                                                            <option data-calling-code="57" data-eu-tax="unknown" value="CO">Colombia</option>
                                                            <option data-calling-code="269" data-eu-tax="unknown" value="KM">Comoros</option>
                                                            <option data-calling-code="242" data-eu-tax="unknown" value="CG">Congo</option>
                                                            <option data-calling-code="243" data-eu-tax="unknown" value="CD">Congo, the Democratic Republic of the</option>
                                                            <option data-calling-code="682" data-eu-tax="unknown" value="CK">Cook Islands</option>
                                                            <option data-calling-code="506" data-eu-tax="unknown" value="CR">Costa Rica</option>
                                                            <option data-calling-code="225" data-eu-tax="unknown" value="CI">Côte d'Ivoire</option>
                                                            <option data-calling-code="385" data-eu-tax="yes" value="HR">Croatia</option>
                                                            <option data-calling-code="53" data-eu-tax="unknown" value="CU">Cuba</option>
                                                            <option data-calling-code="357" data-eu-tax="yes" value="CY">Cyprus</option>
                                                            <option data-calling-code="420" data-eu-tax="yes" value="CZ">Czech Republic</option>
                                                            <option data-calling-code="45" data-eu-tax="yes" value="DK">Denmark</option>
                                                            <option data-calling-code="253" data-eu-tax="unknown" value="DJ">Djibouti</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="DM">Dominica</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="DO">Dominican Republic</option>
                                                            <option data-calling-code="593" data-eu-tax="unknown" value="EC">Ecuador</option>
                                                            <option data-calling-code="20" data-eu-tax="unknown" value="EG">Egypt</option>
                                                            <option data-calling-code="503" data-eu-tax="unknown" value="SV">El Salvador</option>
                                                            <option data-calling-code="240" data-eu-tax="unknown" value="GQ">Equatorial Guinea</option>
                                                            <option data-calling-code="291" data-eu-tax="unknown" value="ER">Eritrea</option>
                                                            <option data-calling-code="372" data-eu-tax="yes" value="EE">Estonia</option>
                                                            <option data-calling-code="251" data-eu-tax="unknown" value="ET">Ethiopia</option>
                                                            <option data-calling-code="500" data-eu-tax="unknown" value="FK">Falkland Islands (Malvinas)</option>
                                                            <option data-calling-code="298" data-eu-tax="unknown" value="FO">Faroe Islands</option>
                                                            <option data-calling-code="679" data-eu-tax="unknown" value="FJ">Fiji</option>
                                                            <option data-calling-code="358" data-eu-tax="yes" value="FI">Finland</option>
                                                            <option data-calling-code="33" data-eu-tax="yes" value="FR">France</option>
                                                            <option data-calling-code="594" data-eu-tax="unknown" value="GF">French Guiana</option>
                                                            <option data-calling-code="689" data-eu-tax="unknown" value="PF">French Polynesia</option>
                                                            <option data-calling-code="262" data-eu-tax="unknown" value="TF">French Southern Territories</option>
                                                            <option data-calling-code="241" data-eu-tax="unknown" value="GA">Gabon</option>
                                                            <option data-calling-code="220" data-eu-tax="unknown" value="GM">Gambia</option>
                                                            <option data-calling-code="995" data-eu-tax="unknown" value="GE">Georgia</option>
                                                            <option data-calling-code="49" data-eu-tax="yes" value="DE">Germany</option>
                                                            <option data-calling-code="233" data-eu-tax="unknown" value="GH">Ghana</option>
                                                            <option data-calling-code="350" data-eu-tax="unknown" value="GI">Gibraltar</option>
                                                            <option data-calling-code="30" data-eu-tax="yes" value="GR">Greece</option>
                                                            <option data-calling-code="299" data-eu-tax="unknown" value="GL">Greenland</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="GD">Grenada</option>
                                                            <option data-calling-code="590" data-eu-tax="unknown" value="GP">Guadeloupe</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="GU">Guam</option>
                                                            <option data-calling-code="502" data-eu-tax="unknown" value="GT">Guatemala</option>
                                                            <option data-calling-code="44" data-eu-tax="unknown" value="GG">Guernsey</option>
                                                            <option data-calling-code="224" data-eu-tax="unknown" value="GN">Guinea</option>
                                                            <option data-calling-code="245" data-eu-tax="unknown" value="GW">Guinea-Bissau</option>
                                                            <option data-calling-code="592" data-eu-tax="unknown" value="GY">Guyana</option>
                                                            <option data-calling-code="509" data-eu-tax="unknown" value="HT">Haiti</option>
                                                            <option data-calling-code="3906" data-eu-tax="unknown" value="VA">Holy See (Vatican City State)</option>
                                                            <option data-calling-code="504" data-eu-tax="unknown" value="HN">Honduras</option>
                                                            <option data-calling-code="852" data-eu-tax="unknown" value="HK">Hong Kong</option>
                                                            <option data-calling-code="36" data-eu-tax="yes" value="HU">Hungary</option>
                                                            <option data-calling-code="354" data-eu-tax="unknown" value="IS">Iceland</option>
                                                            <option data-calling-code="91" data-eu-tax="unknown" value="IN">India</option>
                                                            <option data-calling-code="62" data-eu-tax="unknown" value="ID">Indonesia</option>
                                                            <option data-calling-code="98" data-eu-tax="unknown" value="IR">Iran, Islamic Republic of</option>
                                                            <option data-calling-code="964" data-eu-tax="unknown" value="IQ">Iraq</option>
                                                            <option data-calling-code="353" data-eu-tax="yes" value="IE">Ireland</option>
                                                            <option data-calling-code="44" data-eu-tax="yes" value="IM">Isle of Man</option>
                                                            <option data-calling-code="972" data-eu-tax="unknown" value="IL">Israel</option>
                                                            <option data-calling-code="39" data-eu-tax="yes" value="IT">Italy</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="JM">Jamaica</option>
                                                            <option data-calling-code="81" data-eu-tax="unknown" value="JP">Japan</option>
                                                            <option data-calling-code="44" data-eu-tax="unknown" value="JE">Jersey</option>
                                                            <option data-calling-code="962" data-eu-tax="unknown" value="JO">Jordan</option>
                                                            <option data-calling-code="7" data-eu-tax="unknown" value="KZ">Kazakhstan</option>
                                                            <option data-calling-code="254" data-eu-tax="unknown" value="KE">Kenya</option>
                                                            <option data-calling-code="686" data-eu-tax="unknown" value="KI">Kiribati</option>
                                                            <option data-calling-code="850" data-eu-tax="unknown" value="KP">Korea, Democratic People's Republic of</option>
                                                            <option data-calling-code="82" data-eu-tax="unknown" value="KR">Korea, Republic of</option>
                                                            <option data-calling-code="965" data-eu-tax="unknown" value="KW">Kuwait</option>
                                                            <option data-calling-code="996" data-eu-tax="unknown" value="KG">Kyrgyzstan</option>
                                                            <option data-calling-code="856" data-eu-tax="unknown" value="LA">Lao People's Democratic Republic</option>
                                                            <option data-calling-code="371" data-eu-tax="yes" value="LV">Latvia</option>
                                                            <option data-calling-code="961" data-eu-tax="unknown" value="LB">Lebanon</option>
                                                            <option data-calling-code="266" data-eu-tax="unknown" value="LS">Lesotho</option>
                                                            <option data-calling-code="231" data-eu-tax="unknown" value="LR">Liberia</option>
                                                            <option data-calling-code="218" data-eu-tax="unknown" value="LY">Libyan Arab Jamahiriya</option>
                                                            <option data-calling-code="423" data-eu-tax="unknown" value="LI">Liechtenstein</option>
                                                            <option data-calling-code="370" data-eu-tax="yes" value="LT">Lithuania</option>
                                                            <option data-calling-code="352" data-eu-tax="yes" value="LU">Luxembourg</option>
                                                            <option data-calling-code="853" data-eu-tax="unknown" value="MO">Macao</option>
                                                            <option data-calling-code="389" data-eu-tax="unknown" value="MK">Macedonia, the former Yugoslav Republic of</option>
                                                            <option data-calling-code="261" data-eu-tax="unknown" value="MG">Madagascar</option>
                                                            <option data-calling-code="265" data-eu-tax="unknown" value="MW">Malawi</option>
                                                            <option data-calling-code="60" data-eu-tax="unknown" value="MY" selected="selected">Malaysia</option>
                                                            <option data-calling-code="960" data-eu-tax="unknown" value="MV">Maldives</option>
                                                            <option data-calling-code="223" data-eu-tax="unknown" value="ML">Mali</option>
                                                            <option data-calling-code="356" data-eu-tax="yes" value="MT">Malta</option>
                                                            <option data-calling-code="692" data-eu-tax="unknown" value="MH">Marshall Islands</option>
                                                            <option data-calling-code="596" data-eu-tax="unknown" value="MQ">Martinique</option>
                                                            <option data-calling-code="222" data-eu-tax="unknown" value="MR">Mauritania</option>
                                                            <option data-calling-code="230" data-eu-tax="unknown" value="MU">Mauritius</option>
                                                            <option data-calling-code="262" data-eu-tax="unknown" value="YT">Mayotte</option>
                                                            <option data-calling-code="52" data-eu-tax="unknown" value="MX">Mexico</option>
                                                            <option data-calling-code="691" data-eu-tax="unknown" value="FM">Micronesia, Federated States of</option>
                                                            <option data-calling-code="373" data-eu-tax="unknown" value="MD">Moldova, Republic of</option>
                                                            <option data-calling-code="377" data-eu-tax="yes" value="MC">Monaco</option>
                                                            <option data-calling-code="976" data-eu-tax="unknown" value="MN">Mongolia</option>
                                                            <option data-calling-code="382" data-eu-tax="unknown" value="ME">Montenegro</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="MS">Montserrat</option>
                                                            <option data-calling-code="212" data-eu-tax="unknown" value="MA">Morocco</option>
                                                            <option data-calling-code="258" data-eu-tax="unknown" value="MZ">Mozambique</option>
                                                            <option data-calling-code="94" data-eu-tax="unknown" value="MM">Myanmar</option>
                                                            <option data-calling-code="264" data-eu-tax="unknown" value="NA">Namibia</option>
                                                            <option data-calling-code="674" data-eu-tax="unknown" value="NR">Nauru</option>
                                                            <option data-calling-code="977" data-eu-tax="unknown" value="NP">Nepal</option>
                                                            <option data-calling-code="31" data-eu-tax="yes" value="NL">Netherlands</option>
                                                            <option data-calling-code="599" data-eu-tax="unknown" value="AN">Netherlands Antilles</option>
                                                            <option data-calling-code="687" data-eu-tax="unknown" value="NC">New Caledonia</option>
                                                            <option data-calling-code="64" data-eu-tax="unknown" value="NZ">New Zealand</option>
                                                            <option data-calling-code="505" data-eu-tax="unknown" value="NI">Nicaragua</option>
                                                            <option data-calling-code="227" data-eu-tax="unknown" value="NE">Niger</option>
                                                            <option data-calling-code="234" data-eu-tax="unknown" value="NG">Nigeria</option>
                                                            <option data-calling-code="683" data-eu-tax="unknown" value="NU">Niue</option>
                                                            <option data-calling-code="672" data-eu-tax="unknown" value="NF">Norfolk Island</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="MP">Northern Mariana Islands</option>
                                                            <option data-calling-code="47" data-eu-tax="unknown" value="NO">Norway</option>
                                                            <option data-calling-code="968" data-eu-tax="unknown" value="OM">Oman</option>
                                                            <option data-calling-code="92" data-eu-tax="unknown" value="PK">Pakistan</option>
                                                            <option data-calling-code="680" data-eu-tax="unknown" value="PW">Palau</option>
                                                            <option data-calling-code="970" data-eu-tax="unknown" value="PS">Palestinian Territory, Occupied</option>
                                                            <option data-calling-code="507" data-eu-tax="unknown" value="PA">Panama</option>
                                                            <option data-calling-code="675" data-eu-tax="unknown" value="PG">Papua New Guinea</option>
                                                            <option data-calling-code="595" data-eu-tax="unknown" value="PY">Paraguay</option>
                                                            <option data-calling-code="51" data-eu-tax="unknown" value="PE">Peru</option>
                                                            <option data-calling-code="63" data-eu-tax="unknown" value="PH">Philippines</option>
                                                            <option data-calling-code="649" data-eu-tax="unknown" value="PN">Pitcairn</option>
                                                            <option data-calling-code="48" data-eu-tax="yes" value="PL">Poland</option>
                                                            <option data-calling-code="351" data-eu-tax="yes" value="PT">Portugal</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="PR">Puerto Rico</option>
                                                            <option data-calling-code="974" data-eu-tax="unknown" value="QA">Qatar</option>
                                                            <option data-calling-code="262" data-eu-tax="unknown" value="RE">Réunion</option>
                                                            <option data-calling-code="40" data-eu-tax="yes" value="RO">Romania</option>
                                                            <option data-calling-code="7" data-eu-tax="unknown" value="RU">Russian Federation</option>
                                                            <option data-calling-code="250" data-eu-tax="unknown" value="RW">Rwanda</option>
                                                            <option data-calling-code="590" data-eu-tax="unknown" value="BL">Saint Barthélemy</option>
                                                            <option data-calling-code="290" data-eu-tax="unknown" value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="KN">Saint Kitts and Nevis</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="LC">Saint Lucia</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="MF">Saint Martin (French part)</option>
                                                            <option data-calling-code="508" data-eu-tax="unknown" value="PM">Saint Pierre and Miquelon</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="VC">Saint Vincent and the Grenadines</option>
                                                            <option data-calling-code="685" data-eu-tax="unknown" value="WS">Samoa</option>
                                                            <option data-calling-code="378" data-eu-tax="unknown" value="SM">San Marino</option>
                                                            <option data-calling-code="239" data-eu-tax="unknown" value="ST">Sao Tome and Principe</option>
                                                            <option data-calling-code="966" data-eu-tax="unknown" value="SA">Saudi Arabia</option>
                                                            <option data-calling-code="221" data-eu-tax="unknown" value="SN">Senegal</option>
                                                            <option data-calling-code="382" data-eu-tax="unknown" value="RS">Serbia</option>
                                                            <option data-calling-code="248" data-eu-tax="unknown" value="SC">Seychelles</option>
                                                            <option data-calling-code="232" data-eu-tax="unknown" value="SL">Sierra Leone</option>
                                                            <option data-calling-code="65" data-eu-tax="unknown" value="SG">Singapore</option>
                                                            <option data-calling-code="421" data-eu-tax="yes" value="SK">Slovakia</option>
                                                            <option data-calling-code="386" data-eu-tax="yes" value="SI">Slovenia</option>
                                                            <option data-calling-code="677" data-eu-tax="unknown" value="SB">Solomon Islands</option>
                                                            <option data-calling-code="252" data-eu-tax="unknown" value="SO">Somalia</option>
                                                            <option data-calling-code="27" data-eu-tax="unknown" value="ZA">South Africa</option>
                                                            <option data-calling-code="34" data-eu-tax="yes" value="ES">Spain</option>
                                                            <option data-calling-code="94" data-eu-tax="unknown" value="LK">Sri Lanka</option>
                                                            <option data-calling-code="249" data-eu-tax="unknown" value="SD">Sudan</option>
                                                            <option data-calling-code="597" data-eu-tax="unknown" value="SR">Suriname</option>
                                                            <option data-calling-code="" data-eu-tax="unknown" value="SJ">Svalbard and Jan Mayen</option>
                                                            <option data-calling-code="268" data-eu-tax="unknown" value="SZ">Swaziland</option>
                                                            <option data-calling-code="46" data-eu-tax="yes" value="SE">Sweden</option>
                                                            <option data-calling-code="41" data-eu-tax="unknown" value="CH">Switzerland</option>
                                                            <option data-calling-code="963" data-eu-tax="unknown" value="SY">Syrian Arab Republic</option>
                                                            <option data-calling-code="886" data-eu-tax="unknown" value="TW">Taiwan</option>
                                                            <option data-calling-code="992" data-eu-tax="unknown" value="TJ">Tajikistan</option>
                                                            <option data-calling-code="255" data-eu-tax="unknown" value="TZ">Tanzania, United Republic of</option>
                                                            <option data-calling-code="66" data-eu-tax="unknown" value="TH">Thailand</option>
                                                            <option data-calling-code="670" data-eu-tax="unknown" value="TL">Timor-Leste</option>
                                                            <option data-calling-code="228" data-eu-tax="unknown" value="TG">Togo</option>
                                                            <option data-calling-code="690" data-eu-tax="unknown" value="TK">Tokelau</option>
                                                            <option data-calling-code="676" data-eu-tax="unknown" value="TO">Tonga</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="TT">Trinidad and Tobago</option>
                                                            <option data-calling-code="216" data-eu-tax="unknown" value="TN">Tunisia</option>
                                                            <option data-calling-code="90" data-eu-tax="unknown" value="TR">Turkey</option>
                                                            <option data-calling-code="993" data-eu-tax="unknown" value="TM">Turkmenistan</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="TC">Turks and Caicos Islands</option>
                                                            <option data-calling-code="688" data-eu-tax="unknown" value="TV">Tuvalu</option>
                                                            <option data-calling-code="256" data-eu-tax="unknown" value="UG">Uganda</option>
                                                            <option data-calling-code="380" data-eu-tax="unknown" value="UA">Ukraine</option>
                                                            <option data-calling-code="971" data-eu-tax="unknown" value="AE">United Arab Emirates</option>
                                                            <option data-calling-code="44" data-eu-tax="yes" value="GB">United Kingdom</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="US">United States</option>
                                                            <option data-calling-code="598" data-eu-tax="unknown" value="UY">Uruguay</option>
                                                            <option data-calling-code="998" data-eu-tax="unknown" value="UZ">Uzbekistan</option>
                                                            <option data-calling-code="678" data-eu-tax="unknown" value="VU">Vanuatu</option>
                                                            <option data-calling-code="58" data-eu-tax="unknown" value="VE">Venezuela, Bolivarian Republic of</option>
                                                            <option data-calling-code="84" data-eu-tax="unknown" value="VN">Viet Nam</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="VG">Virgin Islands, British</option>
                                                            <option data-calling-code="1" data-eu-tax="unknown" value="VI">Virgin Islands, U.S.</option>
                                                            <option data-calling-code="681" data-eu-tax="unknown" value="WF">Wallis and Futuna</option>
                                                            <option data-calling-code="" data-eu-tax="unknown" value="EH">Western Sahara</option>
                                                            <option data-calling-code="967" data-eu-tax="unknown" value="YE">Yemen</option>
                                                            <option data-calling-code="260" data-eu-tax="unknown" value="ZM">Zambia</option>
                                                            <option data-calling-code="263" data-eu-tax="unknown" value="ZW">Zimbabwe</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="col-md-offset-5 col-md-8">
                                                        {{ Form::submit('Save', array('class' => 'btn btn-red')) }} 
                                                        <!-- <a href="#" class="btn btn-red">Save &nbsp;<i class="fa fa-floppy-o"></i></a>&nbsp;  -->
                                                        <a href="#" data-dismiss="modal" class="btn btn-green">Cancel &nbsp;<i class="glyphicon glyphicon-ban-circle"></i></a> 
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="modal-delete-selected" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                        <h4 id="modal-login-label4" class="modal-title"><a href=""><i class="fa fa-exclamation-triangle"></i></a> Are you sure you want to delete the selected item(s)? </h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <strong>#B-000001-MY:</strong> Hock Lim<br/>
                                            <strong>Company:</strong> Webqom Technologies Sdn Bhd 
                                        </p>
                                        <div class="form-actions">
                                            <div class="col-md-offset-4 col-md-8"> <a href="#" class="btn btn-red">Yes &nbsp;<i class="fa fa-check"></i></a>&nbsp; <a href="#" data-dismiss="modal" class="btn btn-green">No &nbsp;<i class="fa fa-times-circle"></i></a> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="modal-delete-all" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                        <h4 id="modal-login-label4" class="modal-title"><a href=""><i class="fa fa-exclamation-triangle"></i></a> Are you sure you want to delete all items? </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-actions">
                                            <div class="col-md-offset-4 col-md-8"> <a href="#" class="btn btn-red">Yes &nbsp;<i class="fa fa-check"></i></a>&nbsp; <a href="#" data-dismiss="modal" class="btn btn-green">No &nbsp;<i class="fa fa-times-circle"></i></a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="form-inline pull-left">
                            <div class="form-group">
                                <select name="select" class="form-control">
                                    <option>10</option>
                                    <option>20</option>
                                    <option>30</option>
                                    <option>50</option>
                                    <option selected="selected">100</option>
                                </select>
                                <label class="control-label">Records per page</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="table-responsive mtl">
                            <table id="example1" class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th width="1%"><input type="checkbox"/></th>
                                        <th>#</th>
                                        <th><a href="#sort by client id">Client ID</a></th>
                                        <th><a href="#sort by customer name">Client Name</a></th>
                                        <th><a href="#sort by email">Email</a></th>
                                        <th><a href="#sort by comapny">Company</a></th>
                                        <th><a href="#sort by registered date">Registered Date</a></th>
                                        <th><a href="#sort by type">Type</a></th>
                                        <th><a href="#sort by status">Status</a></th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox"/></td>
                                        <td>2</td>
                                        <td>B-000001-MY</td>
                                        <td>Hock Lim</td>
                                        <td><a href="mailto:hock@webqom.com">hock@webqom.com</a></td>
                                        <td>Webqom Technologies Sdn Bhd</td>
                                        <td>14:25</td>
                                        <td>Business</td>
                                        <td><span class="label label-xs label-success">Active</span></td>
                                        <td>
                                            <a href="client_edit.html" data-hover="tooltip" data-placement="top" title="Edit"><span class="label label-sm label-success"><i class="fa fa-pencil"></i></span></a> <a href="#" data-hover="tooltip" data-placement="top" title="Delete" data-target="#modal-delete-2" data-toggle="modal"><span class="label label-sm label-red"><i class="fa fa-trash-o"></i></span></a>
                                            <div id="modal-delete-2" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                            <h4 id="modal-login-label4" class="modal-title"><a href=""><i class="fa fa-exclamation-triangle"></i></a> Are you sure you want to delete this client? </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>
                                                                <strong>#B-000001-MY:</strong> Hock Lim <br/>
                                                                <strong>Company:</strong> Webqom Technologies Sdn Bhd 
                                                            </p>
                                                            <div class="form-actions">
                                                                <div class="col-md-offset-4 col-md-8"> <a href="#" class="btn btn-red">Yes &nbsp;<i class="fa fa-check"></i></a>&nbsp; <a href="#" data-dismiss="modal" class="btn btn-green">No &nbsp;<i class="fa fa-times-circle"></i></a> </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox"/></td>
                                        <td>1</td>
                                        <td>I-000001-MY</td>
                                        <td>Danny Chan</td>
                                        <td><a href="mailto:danny@webqom.com">danny@webqom.com</a></td>
                                        <td>-</td>
                                        <td>20th Nov 2015<br/>14:25</td>
                                        <td>Individual</td>
                                        <td><span class="label label-xs label-red">Inactive</span></td>
                                        <td>
                                            <a href="client_edit.html" data-hover="tooltip" data-placement="top" title="Edit"><span class="label label-sm label-success"><i class="fa fa-pencil"></i></span></a> <a href="#" data-hover="tooltip" data-placement="top" title="Delete" data-target="#modal-delete-1" data-toggle="modal"><span class="label label-sm label-red"><i class="fa fa-trash-o"></i></span></a>
                                            <div id="modal-delete-1" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                            <h4 id="modal-login-label4" class="modal-title"><a href=""><i class="fa fa-exclamation-triangle"></i></a> Are you sure you want to delete this client? </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>
                                                                <strong>#I-000001-MY:</strong> Danny Chan<br/>
                                                                <strong>Company:</strong> - 
                                                            </p>
                                                            <div class="form-actions">
                                                                <div class="col-md-offset-4 col-md-8"> <a href="#" class="btn btn-red">Yes &nbsp;<i class="fa fa-check"></i></a>&nbsp; <a href="#" data-dismiss="modal" class="btn btn-green">No &nbsp;<i class="fa fa-times-circle"></i></a> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10"></td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="tool-footer text-right">
                                <p class="pull-left">Showing 1 to 10 of 57 entries</p>
                                <ul class="pagination pagination mtm mbm">
                                    <li class="disabled"><a href="#">&laquo;</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">&raquo;</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    {!! Html::script('assets/vendors/metisMenu/jquery.metisMenu.js') !!}
    {!! Html::script('assets/vendors/slimScroll/jquery.slimscroll.js') !!}
    {!! Html::script('assets/vendors/jquery-cookie/jquery.cookie.js') !!}
    {!! Html::script('assets/js/jquery.menu.js') !!}
    {!! Html::script('assets/vendors/jquery-pace/pace.min.js') !!}
    
    {!! Html::script('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}
    {!! Html::script('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') !!}
    {!! Html::script('assets/vendors/moment/moment.js') !!}
    {!! Html::script('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}
    {!! Html::script('assets/vendors/bootstrap-clockface/js/clockface.js') !!}
    {!! Html::script('assets/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js') !!}
    {!! Html::script('assets/vendors/bootstrap-switch/js/bootstrap-switch.min.js') !!}
    {!! Html::script('assets/vendors/jquery-maskedinput/jquery-maskedinput.js') !!}
    {!! Html::script('assets/js/form-components.js') !!}
    
    {!! Html::script('assets/vendors/tinymce/js/tinymce/tinymce.min.js') !!}
    {!! Html::script('assets/vendors/ckeditor/ckeditor.js') !!}
    {!! Html::script('assets/js/ui-tabs-accordions-navs.js') !!}
    
    {!! Html::script('assets/js/main.js') !!}
    {!! Html::script('assets/js/holder.js') !!}
@stop              