<?php

function pre() {
    
    $args = func_get_args();
    if($args) {
        foreach ($args as $key => $arg) {
            echo "<pre>";
            print_r($arg);
            echo "</pre>";
        }
    }
}

function pred() {
    
     call_user_func_array('pre', func_get_args());
     die();
}

function ver() {
    
    $args = func_get_args();
    if($args) {
        foreach ($args as $key => $arg) {
            echo "<pre>";
            var_dump($arg);
            echo "</pre>";
        }
    }
}

function verd() {
    
     call_user_func_array('ver', func_get_args());
     die();
}

function apiCall($action, $apiUrl = '') {
    
    if($apiUrl === '') {
        $apiUrl = \Config::get('app.api_url');
    }
    
    if($action != '') {
        $apiFullUrl =  $apiUrl.'/api/api.php?api_key=63a45c951634222c66af530fdb517b8f&action='.$action;
    } else {
        $apiFullUrl = $apiUrl.'/api/api.php?api_key=63a45c951634222c66af530fdb517b8f';
    }
    
    return $apiFullUrl;
}

/**
 * Return full images url.
 * 
 * @param string $imgName
 * @return string
 */
function imgUrl($imgName)
{
    return asset('assets/images/'. $imgName);
}